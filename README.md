# WordPress Backend Challenge

Desafio WordPress para programadores back-end interessados em trabalhar na Madein Web.

## Introdução

Desenvolva um Tema em WordPress utilizando as funções Nativas.Parte do html e css já foi está sendo disponibilizado, theme MarkUps-dailyShop, desenvolva o tema seguindo o html deste tema. Aplica algumas animações se preferir (caso aplique a animação será avaliado).

## Escopo

É necessário que o tema habilite um novo tipo de conteúdo no WordPress chamado Minha Moda com os campos de Título, Editor clássico do WordPress, Resumo e Imagem destacada e uma taxonomy denominada Fashion Day.

Na single da Minha Moda, deve ser exibido as informações da postagem, data autor e da coleção.

Ainda na single Minha Moda após todo o conteúdo, deve ser exibido um formulário com os campos de Nome e E-mail(não é um campo para comentário). O submit desse formulário deve enviar os dados capturados sem o recarregamento da página, consumindo uma rota na WP Rest Api para salvá-los em uma tabela personalizada do banco de dados que deve armazenar em cada registro além do nome e e-mail preenchidos, a data e hora do preenchimento e tema de interesse. Não se esqueça de apresentar feedbacks das ações do formulário para o usuário.

**Desafio:**
Se aceitar o desafio, É necessário que o administrador possa ver na tela de edição uma listagem com os dados (Nome, e-mail, data e hora) dos interessados em cada Moda. Na tela de listagem dos Modas na administração do WordPress seria interessante a inclusão de uma coluna para mostrar o número de interessados por Moda mensionada pelo autor.

**Requisitos Avaliados:**:

- Funcionalidade
- Uso correto de hooks, funções e classes do WordPress
- Segurança
- Performance
- Organização de Código

## Instruções

1. Efetue um clone deste repositório e crie um branch com o seu nome e sobrenome. (exemplo: maycon-queiroz)
2. Após finalizar o desafio, crie um Pull Request ou envie a branch para o repositório.
3. Aguarde alguém da nossa equipe realizar o code review.

## Pré-requisitos

- PHP >= 7.4
- WordPress mais recente quando da data da realização do desafio
- Orientado a objetos

## Dúvidas

Em caso de dúvidas, crie uma issue.
